//
//  ViewController.swift
//  Emoji Dictionary
//
//  Created by Bryan Irvin on 8/22/17.
//  Copyright © 2017 Bryan Irvin. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var mojiTable: UITableView!
    
    var mojis:Array = ["😃","🤡","😜","😍","😡","☹️","😛","🤑","😘","☺️"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        mojiTable.dataSource = self
        mojiTable.delegate = self
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mojis.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.deselectRow(at: indexPath, animated: true)
        let cell = UITableViewCell()
        cell.textLabel?.text = mojis[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let emoji = mojis[indexPath.row]
        performSegue(withIdentifier: "moveSegway", sender: emoji)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let defVC = segue.destination as! DefinitionViewController
        defVC.emoji = sender as! String
        if let ndx = mojis.index(of: defVC.emoji) {
           defVC.inx = ndx
        }
        
//        guard let ndx = mojis.index(of: defVC.emoji) else {
//            return
//        }
//        defVC.inx = ndx
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

