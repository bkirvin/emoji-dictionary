//
//  DefinitionViewController.swift
//  Emoji Dictionary
//
//  Created by Bryan Irvin on 8/22/17.
//  Copyright © 2017 Bryan Irvin. All rights reserved.
//

import UIKit

class DefinitionViewController: UIViewController {
    
    @IBOutlet weak var defenitionLabel: UILabel!
    
    @IBOutlet weak var emojiLabel: UILabel!
    
    var emoji:String = "NO EMOJI"
    var definitions:Array = ["I am all smiles", "I want to make balloon animals", "I'm goofy enough to lick that", "My eyes are in love", "Now you've done it!", "I'm not happy", "Aaaaahhhhhhh", "I've got money coming out my wazoo", "Catch it if you can", "Flattery will get you everywhere"]
    var inx = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        emojiLabel.text = emoji
        
        defenitionLabel.text = definitions[inx]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 

}
